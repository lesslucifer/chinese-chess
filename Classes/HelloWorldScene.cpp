#include "HelloWorldScene.h"
#include "LongPollingComet/LongPollingComet.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace Comet;

//#define LOCAL_COMET

#ifndef LOCAL_COMET
#define POLL "http://demolaptrinhc.esy.es/comet/Poll.php"
#define PUSH "http://demolaptrinhc.esy.es/comet/Publish.php"
#else
#define POLL "http://localhost/ChineseChessBackend/Poll.php"
#define PUSH "http://localhost/ChineseChessBackend/Publish.php"
#endif

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }

	mPublish = nullptr;
	mComet = nullptr;
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto *lblInfo = Label::create();
	lblInfo->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
	lblInfo->setAlignment(TextHAlignment::CENTER, TextVAlignment::TOP);
	lblInfo->setPosition(visibleSize.width / 2, visibleSize.height - 10);
	lblInfo->setString(StringUtils::format("Poll: %s\nPush: %s", POLL, PUSH));
	addChild(lblInfo);

	auto *txtPollChannel = ui::TextField::create();
	txtPollChannel->setPlaceHolder("Poll channel");
	txtPollChannel->setPosition(Vec2(100, 100));
	addChild(txtPollChannel);

	auto *lblResult = Label::create();
	lblResult->setPosition(500, 100);
	addChild(lblResult);

	auto *pollButton = ui::Button::create();
	pollButton->setTitleText("Poll");
	pollButton->setPosition(Vec2(300, 100));
	pollButton->addClickEventListener([=](Ref*) {
		if (mComet != nullptr)
			return;

		mComet = LongPolling::create(txtPollChannel->getString(),
			POLL,
			PUSH);
		mComet->retain();
		mComet->start();

		this->schedule(CC_CALLBACK_1(LongPolling::runPolling, mComet), "polling");
		mComet->OnReceivedPacket = [=](LongPolling::Packet *p)
		{
			string s((char *) p->getBytes(), p->getSize());
			lblResult->setString(s);
		};
	});
	addChild(pollButton);

	auto *txtPushChannel = ui::TextField::create();
	txtPushChannel->setPlaceHolder("Publish channel");
	txtPushChannel->setPosition(Vec2(100, 300));
	addChild(txtPushChannel);

	auto *txtPushContent = ui::TextField::create();
	txtPushContent->setPlaceHolder("Publish content");
	txtPushContent->setPosition(Vec2(300, 300));
	addChild(txtPushContent);

	auto *pushButton = ui::Button::create();
	pushButton->setTitleText("Push");
	pushButton->setPosition(Vec2(500, 300));
	pushButton->addClickEventListener([=](Ref*) {
		if (mPublish == nullptr)
		{
			mPublish = LongPolling::create(txtPushChannel->getString(),
				POLL,
				PUSH);
			mPublish->retain();
		}

		mPublish->publish((unsigned char *) txtPushContent->getString().c_str(), txtPushContent->getStringLength());
	});
	addChild(pushButton);

    return true;
}


void HelloWorld::menuCloseCallback(Ref* pSender)
{
	string data = "abc";
	mComet->publish((unsigned char*) data.c_str(), data.size());
}
