#include "PieceMoveLogic.h"
#include "Piece.h"

namespace xq
{
	class HorseMoves : public PieceMoveLogic
	{
	public:
		virtual void getMoves(CREF(Index2D) pos,
			CREF(Board) board,
			_out_ vector<Index2D> &toIndices) const
		{
			const Piece *p = board[pos];

			CCASSERT(p && p->getType() == PieceType::HORSE, "");

			addByRule(pos.mod(2, 1), pos.modRow(1), p, board, toIndices);
			addByRule(pos.mod(2, -1), pos.modRow(1), p, board, toIndices);
			addByRule(pos.mod(-2, 1), pos.modRow(-1), p, board, toIndices);
			addByRule(pos.mod(-2, -1), pos.modRow(-1), p, board, toIndices);
			addByRule(pos.mod(1, 2), pos.modCol(1), p, board, toIndices);
			addByRule(pos.mod(-1, 2), pos.modCol(1), p, board, toIndices);
			addByRule(pos.mod(1, -2), pos.modCol(-1), p, board, toIndices);
			addByRule(pos.mod(-1, -2), pos.modCol(-1), p, board, toIndices);
		}

		virtual void addByRule(CREF(Index2D) p, CREF(Index2D) leg,
			const Piece *piece,
			CREF(Board) _board,
			_out_ vector<Index2D> &out) const
		{
			if (_board.contains(p) && !_board[leg] && !isBlocked(piece, _board, p))
			{
				out.push_back(p);
			}
		}
	};

	PieceMoveLogic* const PieceMoveLogic::HORSE = new HorseMoves;
}