#include "PieceMoveLogic.h"
#include "Piece.h"

namespace xq
{
	class ElephantMoves : public PieceMoveLogic
	{
	public:
		virtual void getMoves(CREF(Index2D) pos,
			CREF(Board) board,
			_out_ vector<Index2D> &toIndices) const
		{
			const Piece *p = board[pos];

			CCASSERT(p && p->getType() == PieceType::ELEPHANT, "");

			addByRule(pos.mod(2, 2), pos.mod(1, 1), p, board, toIndices);
			addByRule(pos.mod(2, -2), pos.mod(1, -1), p, board, toIndices);
			addByRule(pos.mod(-2, 2), pos.mod(-1, 1), p, board, toIndices);
			addByRule(pos.mod(-2, -2), pos.mod(-1, -1), p, board, toIndices);
		}

		virtual void addByRule(CREF(Index2D) p, CREF(Index2D) eye,
			const Piece *piece,
			CREF(Board) _board,
			_out_ vector<Index2D> &out) const
		{
			if (_board.contains(p) && p.row < 5 && !_board[eye] && !isBlocked(piece, _board, p))
			{
				out.push_back(p);
			}
		}
	};

	PieceMoveLogic* const PieceMoveLogic::ELEPHANT = new ElephantMoves;
}