#pragma once
#include "ccHelp.h"
#include "Piece.h"
#include <memory>

USING_CC_HELP;
using namespace std;

namespace xq
{
	class /*interface*/ Board
	{
	public:
		virtual bool contains(uint row, uint col) const = 0;
		virtual const Piece* get(uint row, uint col) const = 0;
		virtual Piece* get(uint row, uint col) = 0;
		virtual void set(uint row, uint col, Piece *piece) = 0;
		virtual Board* getOppositeBoard() = 0;
		virtual const Board* getOppositeBoard() const = 0;
		virtual uint nRows() const = 0;
		virtual uint nCols() const = 0;

		inline void set(CREF(Index2D) idx, Piece *piece) {return set(idx.row, idx.col, piece);}

		inline const Piece* operator[](CREF(Index2D) idx) const {return get(idx.row, idx.col);}
		inline Piece* operator[](CREF(Index2D) idx) {return get(idx.row, idx.col);}

		inline bool contains(CREF(Index2D) idx) const {return contains(idx.row, idx.col);}

		inline const Piece* get(CREF(Index2D) idx) const {return get(idx.row, idx.col);}
		inline Piece* get(CREF(Index2D) idx) {return get(idx.row, idx.col);}

		static Board* createBoard();
	};
}