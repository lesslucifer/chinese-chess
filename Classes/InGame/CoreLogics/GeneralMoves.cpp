#include "PieceMoveLogic.h"
#include "Piece.h"

namespace xq
{
	class GeneralMoves : public PieceMoveLogic
	{
	public:
		virtual void getMoves(CREF(Index2D) pos,
			CREF(Board) board,
			_out_ vector<Index2D> &toIndices) const
		{
			CCASSERT(inPalace(pos), "General can only in palace!");

			const Piece *p = board[pos];

			CCASSERT(p && p->getType() == PieceType::GENERAL, "");

			addByRule(pos.mod(1, 0), p, board, toIndices);
			addByRule(pos.mod(-1, 0), p, board, toIndices);
			addByRule(pos.mod(0, 1), p, board, toIndices);
			addByRule(pos.mod(0, -1), p, board, toIndices);
		}

	private:
		void addByRule(CREF(Index2D) p,
			const Piece *piece,
			CREF(Board) _board,
			_out_ vector<Index2D> &out) const
		{
			if (inPalace(p) && !isBlocked(piece, _board, p))
			{
				out.push_back(p);
			}
		}
	};

	PieceMoveLogic* const PieceMoveLogic::GENERAL = new GeneralMoves;
}