#pragma once
#include "ccHelp.h"

USING_CC_HELP;

namespace xq
{
	class __Unused_PieceType
	{
	public:
		// defined
		static __Unused_PieceType * const EMPTY;
		static __Unused_PieceType * const SOLDIER;
		static __Unused_PieceType * const ADVISOR;
		static __Unused_PieceType * const ELEPHANT;
		static __Unused_PieceType * const HORSE;
		static __Unused_PieceType * const CANNON;
		static __Unused_PieceType * const CHARIOT;
		static __Unused_PieceType * const GENERAL;

	private:
		// prevent create other type
		__Unused_PieceType(byte uniqueid, uint score, string sid);

	public:
		CC_SYNTHESIZE_READONLY(uint, mScore, Score);
		CC_SYNTHESIZE_READONLY(string, mIdentifier, SID);
		CC_SYNTHESIZE_READONLY(byte, mUniqueID, UniqueID);
	};

	// use this, you have not to create new PieceType
	typedef __Unused_PieceType PieceType;

}