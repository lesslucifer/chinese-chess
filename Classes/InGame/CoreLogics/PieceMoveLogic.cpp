#include "PieceMoveLogic.h"
#include "Piece.h"
#include "ccHelp.h"

namespace xq
{
	PieceMoveLogic::PieceMoveLogic() {}

	bool PieceMoveLogic::isBlocked(const Piece *p, CREF(Board) board, CREF(Index2D) idx)
	{
		return board[idx] && board[idx]->getTeam() == p->getTeam();
	}

	bool PieceMoveLogic::inPalace(CREF(Index2D) p)
	{
		return p.row < 3 && p.col >= 3 && p.col <= 5;
	}

	PieceMoveLogic* PieceMoveLogic::getLogic(PieceType *type)
	{
		if (type == PieceType::GENERAL)
		{
			return PieceMoveLogic::GENERAL;
		}
		else if (type == PieceType::ADVISOR)
		{
			return PieceMoveLogic::ADVISOR;
		}
		else if (type == PieceType::ELEPHANT)
		{
			return PieceMoveLogic::ELEPHANT;
		}
		else if (type == PieceType::HORSE)
		{
			return PieceMoveLogic::HORSE;
		}
		else if (type == PieceType::CANNON)
		{
			return PieceMoveLogic::CANNON;
		}
		else if (type == PieceType::CHARIOT)
		{
			return PieceMoveLogic::CHARIOT;
		}
		else if (type == PieceType::SOLDIER)
		{
			return PieceMoveLogic::SOLDIER;
		}

		return nullptr;
	}
}