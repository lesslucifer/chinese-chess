#include "PieceMoveLogic.h"
#include "Piece.h"

namespace xq
{
	class CannonMoves : public PieceMoveLogic
	{
	public:
		virtual void getMoves(CREF(Index2D) pos,
			CREF(Board) board,
			_out_ vector<Index2D> &toIndices) const
		{
			static Index2D dirs[] = {
				Index2D(1, 0),
				Index2D(-1, 0),
				Index2D(0, 1),
				Index2D(0, -1)
			};

			const Piece *cannon = board[pos];

			CCASSERT(cannon && cannon->getType() == PieceType::CANNON, "");

			for (CREF(auto) dir : dirs)
			{
				Index2D p = pos + dir;
				bool hasPlatform = false;

				while (board.contains(p))
				{
					auto *piece = board[p];
					if (piece)
					{
						if (!hasPlatform)
						{
							hasPlatform = true;
						}
						else
						{
							if (piece->getTeam() != cannon->getTeam())
							{
								toIndices.push_back(p);
							}

							break;
						}
					}
					else if (!hasPlatform)
					{
						toIndices.push_back(p);
					}

					p += dir;
				}
			}
		}
	};

	PieceMoveLogic* const PieceMoveLogic::CANNON = new CannonMoves;
}