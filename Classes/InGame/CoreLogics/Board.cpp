#include "Board.h"

namespace xq
{
	class OppositeBoard : public Board
	{
	private:
		Board *mOpposite;

	public:
		OppositeBoard(Board *opposite)
			: mOpposite(opposite) {}

	private:
		void rev(uint &r, uint &c) const
		{
			r = nRows() - r - 1;
			c = nCols() - c - 1;
		}

	public:
		virtual bool contains(uint row, uint col) const override
		{
			rev(row, col);
			return mOpposite->contains(row, col);
		}

		virtual const Piece* get(uint row, uint col) const override
		{
			rev(row, col);
			return mOpposite->get(row, col);
		}

		virtual Piece* get(uint row, uint col)  override
		{
			rev(row, col);
			return mOpposite->get(row, col);
		}

		virtual void set(uint row, uint col, Piece *piece) override
		{
			rev(row, col);
			mOpposite->set(row, col, piece);
		}

		virtual Board* getOppositeBoard() override
		{
			return mOpposite;
		}

		virtual const Board* getOppositeBoard() const override
		{
			return mOpposite;
		}

		virtual uint nRows() const override
		{
			return mOpposite->nRows();
		}

		virtual uint nCols() const override
		{
			return mOpposite->nCols();
		}
	};

	class BoardImpl : public Board
	{
	protected:
		Array2D<Piece> mData;
		shared_ptr<Board> mOppositeBoard;

	public:
		BoardImpl() :
			mData(10, 9),
			mOppositeBoard(new OppositeBoard(this))
		{

		}

		virtual bool contains(uint row, uint col) const override
		{
			return mData.isContains(row, col);
		}

		virtual const Piece* get(uint row, uint col) const override
		{
			return mData.get(row, col);
		}

		virtual Piece* get(uint row, uint col) override
		{
			return mData.get(row, col);
		}

		virtual void set(uint row, uint col, Piece *piece) override
		{
			mData.put(piece, row, col);
		}

		virtual Board* getOppositeBoard() override
		{
			return mOppositeBoard.get();
		}

		virtual const Board* getOppositeBoard() const override
		{
			return mOppositeBoard.get();
		}

		virtual uint nRows() const override
		{
			return mData.nRows();
		}

		virtual uint nCols() const override
		{
			return mData.nCols();
		}
	};

	Board* Board::createBoard()
	{
		return new BoardImpl;
	}
}