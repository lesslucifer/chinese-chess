#include "BoardLogic.h"
#include "Piece.h"
#include "PieceMoveLogic.h"

namespace xq
{
	BoardLogic::BoardLogic(Board *board, Team team)
		: mBoard(board), mTeam(team),
		mPlayState(WAIT_SELECT), mSelectedIndex(0, 0)
	{}

	void BoardLogic::setupBoard()
	{
		clearBoard();

		setupOneTeam(LIGHT);
		setupOneTeam(DARK);

		OnBoardLoaded(mBoard);
	}

	void BoardLogic::boardTouched(Index2D idx)
	{
		if (mPlayState == WAIT_SELECT)
		{
			if (auto *piece = mBoard->get(idx))
			{
				mPlayState = SELECTED;
				mSelectedIndex = idx;
				OnPieceSelected(piece, idx);
			}
		}
		else if (mPlayState == SELECTED)
		{
			auto *piece = mBoard->get(mSelectedIndex);
			auto *targetPiece = mBoard->get(idx);
			if (piece)
			{
				vector<Index2D> moves;
				piece->getMoveLogic()->getMoves(mSelectedIndex, *mBoard, moves);

				auto it = std::find(moves.begin(), moves.end(), idx);
				if (it != moves.end())
				{
					piece->retain();
					mBoard->set(mSelectedIndex, nullptr);

					if (targetPiece)
					{
						CCASSERT(targetPiece->getTeam() != piece->getTeam(),
							"Cannot move to blocked cell");

						targetPiece->retain();
						mBoard->set(idx, nullptr);
						OnPieceKilled(targetPiece, idx);
						targetPiece->release();
					}

					mBoard->set(idx, piece);
					OnPieceMoved(piece, mSelectedIndex, idx);
					piece->release();

					mPlayState = WAIT_SELECT;
				}
				else
				{
					piece->retain();
					OnPieceUnselected(piece, mSelectedIndex);
					piece->release();
					mPlayState = WAIT_SELECT;

					if (targetPiece && targetPiece->getTeam() == piece->getTeam())
					{
						mPlayState = SELECTED;
						mSelectedIndex = idx;
						OnPieceSelected(targetPiece, idx);
					}
				}
			}
		}
	}

	void BoardLogic::clearBoard()
	{
		for (uint r = 0; r < mBoard->nRows(); ++r)
		{
			for (uint c = 0; c < mBoard->nCols(); ++c)
			{
				mBoard->set(r, c, nullptr);
			}
		}
	}

	void BoardLogic::setupOneTeam(Team team)
	{
		Board *board = mBoard;
		if (team != mTeam)
		{
			board = board->getOppositeBoard();
		}

		// setup
		// align with general's position
		Index2D generalPos(0, 4);

		// general
		board->set(generalPos, Piece::create(team, PieceType::GENERAL));

		// advisors
		board->set(generalPos.modCol(1), Piece::create(team, PieceType::ADVISOR));
		board->set(generalPos.modCol(-1), Piece::create(team, PieceType::ADVISOR));

		// elephants
		board->set(generalPos.modCol(2), Piece::create(team, PieceType::ELEPHANT));
		board->set(generalPos.modCol(-2), Piece::create(team, PieceType::ELEPHANT));

		// horses
		board->set(generalPos.modCol(3), Piece::create(team, PieceType::HORSE));
		board->set(generalPos.modCol(-3), Piece::create(team, PieceType::HORSE));

		// chariots
		board->set(generalPos.modCol(4), Piece::create(team, PieceType::CHARIOT));
		board->set(generalPos.modCol(-4), Piece::create(team, PieceType::CHARIOT));

		// middle of cannon's row
		Index2D cannonsRowMiddle(2, 4);

		// cannons
		board->set(cannonsRowMiddle.modCol(3), Piece::create(team, PieceType::CANNON));
		board->set(cannonsRowMiddle.modCol(-3), Piece::create(team, PieceType::CANNON));

		// middle of soldier's row
		Index2D soldiersRowMiddle(3, 4);

		// soldier
		board->set(soldiersRowMiddle, Piece::create(team, PieceType::SOLDIER));
		board->set(soldiersRowMiddle.modCol(2), Piece::create(team, PieceType::SOLDIER));
		board->set(soldiersRowMiddle.modCol(-2), Piece::create(team, PieceType::SOLDIER));
		board->set(soldiersRowMiddle.modCol(4), Piece::create(team, PieceType::SOLDIER));
		board->set(soldiersRowMiddle.modCol(-4), Piece::create(team, PieceType::SOLDIER));
	}
}