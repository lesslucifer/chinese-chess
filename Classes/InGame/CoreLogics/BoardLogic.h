#pragma once
#include "Board.h"

namespace xq
{
	class BoardLogic
	{
	public:
		BoardLogic(Board *board, Team team);

		void setupBoard();
		void boardTouched(Index2D idx);

		CC_SYNTHESIZE_READONLY(Board*, mBoard, Board);
		CC_SYNTHESIZE_READONLY(Team, mTeam, Team);

		ccHelp::Event<void(Board *)> OnBoardLoaded;
		ccHelp::Event<void(Piece *, CREF(Index2D))> OnPieceSelected;
		ccHelp::Event<void(Piece *, CREF(Index2D))> OnPieceUnselected;
		ccHelp::Event<void(Piece *, CREF(Index2D) /*from*/, CREF(Index2D) /*to*/)> OnPieceMoved;
		ccHelp::Event<void(Piece*, CREF(Index2D))> OnPieceKilled;

	private:
		void clearBoard();
		void setupOneTeam(Team team);

		enum PlayState
		{
			WAIT_SELECT,
			SELECTED
		};
		PlayState mPlayState;
		Index2D mSelectedIndex;
	};
}