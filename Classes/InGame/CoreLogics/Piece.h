#pragma once
#include "cocos2d.h"
#include "Team.h"
#include "PieceType.h"

namespace xq
{
	class PieceMoveLogic;

	// Piece is an entity
	class Piece : public Ref
	{
	public:

		// declares
		CC_SYNTHESIZE_READONLY(Team, mTeam, Team);
		CC_SYNTHESIZE_READONLY(PieceType*, mType, Type);
		CC_SYNTHESIZE_READONLY(PieceMoveLogic*, mMoveLogic, MoveLogic);

		static Piece* create(Team team, PieceType *type);
	};
}