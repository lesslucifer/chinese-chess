#include "PieceMoveLogic.h"
#include "Piece.h"

namespace xq
{
	class ChariotMoves : public PieceMoveLogic
	{
	public:
		virtual void getMoves(CREF(Index2D) pos,
			CREF(Board) board,
			_out_ vector<Index2D> &toIndices) const
		{
			static Index2D dirs[] = {
				Index2D(1, 0),
				Index2D(-1, 0),
				Index2D(0, 1),
				Index2D(0, -1)
			};

			const Piece *chariot = board[pos];

			CCASSERT(chariot && chariot->getType() == PieceType::CHARIOT, "");

			for (CREF(auto) dir : dirs)
			{
				Index2D p = pos + dir;

				while (board.contains(p))
				{
					if (auto *piece = board[p])
					{
						if (piece->getTeam() != chariot->getTeam())
						{
							toIndices.push_back(p);
						}

						break;
					}
					else
					{
						toIndices.push_back(p);
					}

					p += dir;
				}
			}
		}
	};

	PieceMoveLogic* const PieceMoveLogic::CHARIOT = new ChariotMoves;
}