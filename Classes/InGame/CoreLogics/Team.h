#pragma once
#include "ccHelp.h"

namespace xq
{
	enum Team
	{
		DARK,
		LIGHT
	};

	class TeamLogic
	{
	public:
		inline static Team opposite(Team t)
		{
			return (t == LIGHT)?DARK:LIGHT;
		}

		inline static string sid(Team t)
		{
			return (t == LIGHT) ? "Light" : "Dark";
		}

		inline static Team fromString(const string &s)
		{
			string ss = ccHelp::Utils::tolower(s);
			if (ss == "light")
			{
				return LIGHT;
			}
			else if (ss == "dark")
			{
				return DARK;
			}

			assert(false);
		}
	};
}