#pragma once
#include "PieceType.h"
#include "Board.h"

namespace xq
{
	class Piece;

	class /*interface*/ PieceMoveLogic
	{
	public:
		static PieceMoveLogic * const SOLDIER;
		static PieceMoveLogic * const ADVISOR;
		static PieceMoveLogic * const ELEPHANT;
		static PieceMoveLogic * const HORSE;
		static PieceMoveLogic * const CANNON;
		static PieceMoveLogic * const CHARIOT;
		static PieceMoveLogic * const GENERAL;

		virtual void getMoves(CREF(Index2D) pos,
			CREF(Board) board,
			_out_ vector<Index2D> &toIndices) const = 0;

		static bool isBlocked(const Piece *p, CREF(Board) board, CREF(Index2D) idx);
		static bool inPalace(CREF(Index2D) p);

		static PieceMoveLogic* getLogic(PieceType *type);
	protected:
		PieceMoveLogic(); // prevent to create other move gen
	};
}