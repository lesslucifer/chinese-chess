#include "PieceType.h"

namespace xq
{
	__Unused_PieceType::__Unused_PieceType(byte uniqueid, uint score, string sid)
		: mScore(score), mUniqueID(uniqueid), mIdentifier(sid) {}

	__Unused_PieceType * const __Unused_PieceType::EMPTY =
		new __Unused_PieceType(0, 0, "Empty");
	__Unused_PieceType * const __Unused_PieceType::SOLDIER =
		new __Unused_PieceType(1, 30, "Soldier");
	__Unused_PieceType * const __Unused_PieceType::ADVISOR =
		new __Unused_PieceType(2, 120, "Advisor");
	__Unused_PieceType * const __Unused_PieceType::ELEPHANT =
		new __Unused_PieceType(3, 120, "Elephant");
	__Unused_PieceType * const __Unused_PieceType::HORSE =
		new __Unused_PieceType(4, 270, "Horse");
	__Unused_PieceType * const __Unused_PieceType::CANNON =
		new __Unused_PieceType(5, 285, "Cannon");
	__Unused_PieceType * const __Unused_PieceType::CHARIOT =
		new __Unused_PieceType(6, 600, "Chariot");
	__Unused_PieceType * const __Unused_PieceType::GENERAL =
		new __Unused_PieceType(7, 6000, "General");
}