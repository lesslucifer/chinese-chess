#include "PieceMoveLogic.h"
#include "Piece.h"

namespace xq
{
	class SoldierMoves : public PieceMoveLogic
	{
	public:
		virtual void getMoves(CREF(Index2D) pos,
			CREF(Board) board,
			_out_ vector<Index2D> &toIndices) const
		{
			const Piece *p = board[pos];
			CCASSERT(p && p->getType() == PieceType::SOLDIER, "");

			auto fw = pos.modRow(1);
			if (fw.row < board.nRows() && !isBlocked(p, board, fw))
			{
				toIndices.push_back(fw);
			}

			if (pos.row >= 5) // crossed river
			{
				auto left = pos.modCol(-1);
				if (left.col < board.nCols() && !isBlocked(p, board, left))
				{
					toIndices.push_back(left);
				}

				auto right = pos.modCol(1);
				if (right.col < board.nCols() && !isBlocked(p, board, right))
				{
					toIndices.push_back(right);
				}
			}
		}
	};

	PieceMoveLogic* const PieceMoveLogic::SOLDIER = new SoldierMoves;
}