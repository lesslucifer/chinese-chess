#include "Piece.h"
#include "PieceMoveLogic.h"

namespace xq
{
	Piece* Piece::create(Team team, PieceType *type)
	{
		Piece *piece = new Piece;
		piece->mTeam = team;
		piece->mType = type;
		piece->mMoveLogic = PieceMoveLogic::getLogic(type); 

		piece->autorelease();

		return piece;
	}
}