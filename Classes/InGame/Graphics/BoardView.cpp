#include "BoardView.h"
#include "PieceMoveLogic.h"

namespace xq
{
	BoardView::BoardView()
		: mBoardLogic(nullptr), mBoard(nullptr)
	{

	}

	bool BoardView::init()
	{
		if (!Layer::init())
			return false;

		mBoardBackground = Sprite::create("Boards/1.png");
		addChild(mBoardBackground, 0, "Background");

		mMoveablePosHighlight = DrawNode::create();
		addChild(mMoveablePosHighlight, 2, "MoveablePen");

		auto *tl = EventListenerTouchOneByOne::create();
		tl->setSwallowTouches(true);
		tl->onTouchBegan = CC_CALLBACK_2(BoardView::TouchBegan, this);
		tl->onTouchEnded = CC_CALLBACK_2(BoardView::TouchEnded, this);
		Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(tl, this);

		ccHelp::LayoutHelper::apply(this, "layouts/BoardView.json");

		return true;
	}

	void BoardView::setBoardLogic(BoardLogic *bLogic)
	{
		mBoardLogic = bLogic;

		mBoardLogic->OnBoardLoaded += CC_CALLBACK_1(BoardView::boardLoaded, this);
		mBoardLogic->OnPieceSelected += CC_CALLBACK_2(BoardView::pieceSelected, this);
		mBoardLogic->OnPieceUnselected += CC_CALLBACK_2(BoardView::pieceUnselected, this);
		mBoardLogic->OnPieceMoved += CC_CALLBACK_3(BoardView::pieceMoved, this);
		mBoardLogic->OnPieceKilled += CC_CALLBACK_2(BoardView::pieceKilled, this);
	}

	void BoardView::boardLoaded(Board *board)
	{
		this->mBoard = board;
		this->clearBoard();
		this->setupPieces();
	}

	void BoardView::pieceSelected(Piece *piece, CREF(Index2D) index)
	{
		this->clearSelectGraphics();

		PieceView *pView = mPieces.at(piece);
		pView->setSelected(true);
		drawMoveablePos(pView, index);
	}

	void BoardView::pieceUnselected(Piece *piece, CREF(Index2D) index)
	{
		this->clearSelectGraphics();
	}

	void BoardView::pieceMoved(Piece *piece, CREF(Index2D) from, CREF(Index2D) to)
	{
		this->clearSelectGraphics();

		auto targetPos = posFromIndex(to);
		PieceView *pView = mPieces.at(piece);

		auto *act = MoveTo::create(0.2f, targetPos);
		pView->runAction(act);
	}

	void BoardView::pieceKilled(Piece *piece, CREF(Index2D) index)
	{
		PieceView *pView = mPieces.at(piece);
		if (pView)
		{
			pView->removeFromParent();
		}
	}

	void BoardView::clearBoard()
	{
		for (auto it : mPieces)
		{
			it.second->removeFromParent();
		}

		mPieces.clear();
	}

	void BoardView::setupPieces()
	{
		for (uint r = 0; r < mBoard->nRows(); ++r)
		{
			for (uint c = 0; c < mBoard->nCols(); ++c)
			{
				if (auto *piece = mBoard->get(r, c))
				{
					PieceView *pView = PieceView::create(piece);
					pView->setPosition(posFromIndex(Index2D(r, c)));
					addChild(pView, 1);
					mPieces.insert(piece, pView);
				}
			}
		}
	}

	Vec2 BoardView::posFromIndex(CREF(Index2D) idx) const
	{
		return Vec2(30 + 60 * idx.col, 30 + 60 * idx.row);
	}

	Index2D BoardView::indexFromPos(CREF(Vec2) p) const
	{
		return Index2D(static_cast<uint>((p.y - 30) / 60),
			static_cast<uint>((p.x - 30) / 60));
	}

	bool BoardView::TouchBegan(Touch *t, cocos2d::Event *ue)
	{
		auto pos = this->convertToNodeSpace(t->getLocation());
		pos += Vec2(30, 30);
		auto index = indexFromPos(pos);
		mBoardLogic->boardTouched(index);
		return true;
	}

	void BoardView::TouchEnded(Touch *t, cocos2d::Event *ue)
	{

	}

	void BoardView::drawMoveablePos(PieceView *pView, Index2D index)
	{
		if (pView)
		{
			Piece *piece = pView->getPiece();
			auto index = indexFromPos(pView->getPosition());
			CCASSERT(piece == mBoard->get(index), "");

			vector<Index2D> moves;
			piece->getMoveLogic()->getMoves(index, *mBoard, moves);

			for (const auto &move : moves)
			{
				mMoveablePosHighlight->drawSolidCircle(posFromIndex(move), 5.f,
					static_cast<float>(2.f * M_PI), 16, Color4F::GREEN);
			}
		}
	}

	void BoardView::clearSelectGraphics()
	{
		mMoveablePosHighlight->clear();
		for (auto it : mPieces)
		{
			it.second->setSelected(false);
		}
	}
}