#include "PieceView.h"

namespace xq
{
	bool PieceView::init(Piece *piece)
	{
		if (!Node::init())
			return false;

		mPiece = piece;
		this->mSelectedFg = Sprite::create("Pieces/SelectedFg.png");
		addChild(mSelectedFg, 0, "SelectedFg");

		string spriteFile = StringUtils::format("Pieces/%s/%s.png",
			TeamLogic::sid(piece->getTeam()).c_str(),
			piece->getType()->getSID().c_str());

		this->mPieceSprite = Sprite::create(spriteFile);
		addChild(mPieceSprite, 1, "Image");

		ccHelp::LayoutHelper::apply(this, "layouts/PieceView.json");

		return true;
	}
}