#pragma once
#include "ccHelp.h"
#include "BoardLogic.h"
#include "PieceView.h"

USING_CC_HELP;
USING_NS_CC;
using namespace std;

namespace xq
{
	class BoardView : public Layer
	{
	public:
		BoardView();

		virtual bool init() override;
		inline virtual void setBoardLogic(BoardLogic *boardLogic);

		virtual void boardLoaded(Board *board);
		virtual void pieceSelected(Piece *piece, CREF(Index2D) index);
		virtual void pieceUnselected(Piece *piece, CREF(Index2D) index);
		virtual void pieceMoved(Piece *piece, CREF(Index2D) from, CREF(Index2D) to);
		virtual void pieceKilled(Piece *piece, CREF(Index2D) index);

	private:
		Sprite *mBoardBackground;
		BoardLogic *mBoardLogic;
		Board *mBoard;
		DrawNode *mMoveablePosHighlight;

		Map<Piece*, PieceView*> mPieces;

	private:
		void setupPieces();
		void clearBoard();


		Vec2 posFromIndex(CREF(Index2D)) const;
		Index2D indexFromPos(CREF(Vec2) p) const;

		bool TouchBegan(Touch *t, cocos2d::Event *ue);
		void TouchEnded(Touch *t, cocos2d::Event *ue);

		void clearSelectGraphics();
		void drawMoveablePos(PieceView *pView, Index2D index);
	public:
		CREATE_FUNC(BoardView);
	};
}