#pragma once
#include "Global.h"
#include "Piece.h"

namespace xq
{
	class PieceView : public Node
	{
	public:
		virtual bool init(Piece *piece);

		inline bool isSelected() const { return mSelectedFg->isVisible(); }
		inline void setSelected(bool _sel) { mSelectedFg->setVisible(_sel); }

		CC_SYNTHESIZE_READONLY(Piece*, mPiece, Piece);

	private:
		Sprite *mPieceSprite;
		Sprite *mSelectedFg;

	public:
		inline static PieceView* create(Piece *piece)
		{
			return ccHelp::Utils::createx<PieceView>(new PieceView, [=](PieceView *p) {
				return p->init(piece);
			});
		}
	};
}