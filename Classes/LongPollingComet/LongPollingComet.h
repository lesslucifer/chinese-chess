#pragma once
#include "cocos2d.h"
#include "network/HttpClient.h"
#include "network/HttpRequest.h"

USING_NS_CC;
using namespace std;
using namespace network;

namespace Comet
{
	class LongPolling : public Ref
	{
	public:
		typedef cocos2d::Data Packet;
		typedef std::function<void(Packet*)> Handler;
		enum State
		{
			IDLE,
			POLLING,
			MUTING
		};

	public:
		inline void start() {mState = MUTING; mTimer = 0;}
		inline void stop() {mState = IDLE;}

		void runPolling(float dt);
		void publish(const unsigned char *data, unsigned int len);

		Handler OnReceivedPacket;
		Handler OnPublishedPacket;

	private:
		inline LongPolling() : mState(IDLE), mTimer(0) {}

		State mState;
		float mTimer;

		void setupPolling();
		void onPollResponse(HttpClient *client, HttpResponse *resp);
		void onPublishResponse(HttpClient *client, HttpResponse *resp);

		// synthesies:
		CC_SYNTHESIZE(string, mPollURL, PollURL);
		CC_SYNTHESIZE(string, mPublishURL, PublishURL);
		CC_SYNTHESIZE(string, mChannel, Channel);
		CC_SYNTHESIZE(float, mTimeOut, TimeOut);
		CC_SYNTHESIZE(float, mMuteTime, MuteTime);

	public:
		static LongPolling* create(
			string channel,
			string pollURL,
			string publishURL,
			float timeOut = 40,
			float muteTime = 4);
	};
}