#include "LongPollingComet.h"

namespace Comet
{
	void LongPolling::runPolling(float dt)
	{
		mTimer -= dt;
		if (mState == MUTING && mTimer <= 0)
		{
			setupPolling();
			mState = POLLING;
			return;
		}
	}

	void LongPolling::publish(const unsigned char *data, unsigned int len)
	{
		CCASSERT(!mChannel.empty(), "Empty channel");
		CCASSERT(!mPublishURL.empty(), "Empty publish URL");

		network::HttpRequest *req = new network::HttpRequest;
		req->setUrl(mPublishURL.c_str());
		req->setResponseCallback(CC_CALLBACK_2(LongPolling::onPublishResponse, this));
		req->setRequestType(HttpRequest::Type::POST);
		char *base64Data;
		base64Encode(data, len, &base64Data);
		string reqData = StringUtils::format("channel=%s&data=%s", mChannel.c_str(), base64Data);
		req->setRequestData(reqData.c_str(), reqData.length());
		HttpClient::getInstance()->sendImmediate(req);
		req->release();
		free(base64Data);

		this->retain();
	}

	void LongPolling::setupPolling()
	{
		CCASSERT(!mChannel.empty(), "Empty channel");
		CCASSERT(!mPollURL.empty(), "Empty poll URL");

		network::HttpRequest *req = new network::HttpRequest;
		req->setUrl(mPollURL.c_str());
		req->setResponseCallback(CC_CALLBACK_2(LongPolling::onPollResponse, this));
		req->setRequestType(HttpRequest::Type::POST);
		string reqData = StringUtils::format("channel=%s", mChannel.c_str());
		req->setRequestData(reqData.c_str(), reqData.length());
		HttpClient::getInstance()->sendImmediate(req);
		req->release();

		this->retain();
	}

	void LongPolling::onPollResponse(HttpClient *client, HttpResponse *resp)
	{
		if (resp->isSucceed())
		{
			if (OnReceivedPacket)
			{
				// resp success, handle the resp
				auto *data = resp->getResponseData();
				unsigned char *decodedData;
				unsigned int len = base64Decode((const unsigned char *) data->data(), data->size(), &decodedData);

				Packet packet;
				packet.fastSet(decodedData, len);
				OnReceivedPacket(&packet);
			}
		}

		// waits mute time & try new connect
		if (mState == POLLING)
		{
			mState = MUTING;
			mTimer = mMuteTime;
		}

		this->release();
	}

	void LongPolling::onPublishResponse(HttpClient *client, HttpResponse *resp)
	{
		if (!OnPublishedPacket)
			return;

		Packet packet;
		packet.copy((unsigned char*) resp->getResponseData()->data(), resp->getResponseData()->size());
		OnPublishedPacket(&packet);

		this->release();
	}

	LongPolling* LongPolling::create(string channel, string pollURL, string publishURL, float timeOut /* = 40 */, float muteTime /* = 4 */)
	{
		LongPolling *longPolling = new LongPolling;
		longPolling->setChannel(channel);
		longPolling->setPollURL(pollURL);
		longPolling->setPublishURL(publishURL);
		longPolling->setTimeOut(timeOut);
		longPolling->setMuteTime(muteTime);

		longPolling->autorelease();

		return longPolling;
	}
}