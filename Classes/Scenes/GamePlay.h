#pragma once
#include <ccHelp/ccHelp.h>
#include "BoardView.h"
#include <memory>

USING_NS_CC;
USING_CC_HELP;

namespace xq
{
	class GamePlayLayer : public cocos2d::Layer
	{
	public:
		// there's no 'id' in cpp, so we recommend returning the class instance pointer
		static cocos2d::Scene* createScene();

		// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
		virtual bool init();

		// implement the "static create()" method manually
		CREATE_FUNC(GamePlayLayer);

	private:
		shared_ptr<Board> mBoard;
		shared_ptr<BoardLogic> mBoardLogic;
		BoardView *mBoardView;
	};
}