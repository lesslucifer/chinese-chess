#include "GamePlay.h"

namespace xq
{
	Scene* GamePlayLayer::createScene()
	{
		Scene *scene = Scene::create();
		scene->addChild(GamePlayLayer::create());

		return scene;
	}

	bool GamePlayLayer::init()
	{
		if (!Layer::init())
		{
			return false;
		}

		auto winSize = Director::getInstance()->getWinSize();

		mBoardView = BoardView::create();
		addChild(mBoardView, 0, "Board");

		LayoutHelper::apply(this, "layouts/GamePlayScene.json");

		//setup boards:
		mBoard = shared_ptr<Board>(Board::createBoard());
		mBoardLogic = shared_ptr<BoardLogic>(new BoardLogic(mBoard.get(), LIGHT));

		mBoardView->setBoardLogic(mBoardLogic.get());

		mBoardLogic->setupBoard();

		return true;
	}
}